package com.example.demo.controller;

import com.example.demo.model.Auth;
import com.example.demo.model.User;
import com.example.demo.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(authService.getAll());
    }
    @PostMapping
    public ResponseEntity<?> add(@RequestBody Auth auth) {
        authService.add(auth);
        return ResponseEntity.ok("Auth is added");
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Auth auth) {
        authService.update(auth);
        return ResponseEntity.ok(auth);
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Auth auth) {
        authService.delete(auth);
        return ResponseEntity.ok("Auth successfully deleted");
    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestHeader("login") String login,
                                   @RequestHeader("pass") String password) {
        String token = authService.login(login,password);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/loginchecking")
    public ResponseEntity<?> LoginCheck(@RequestHeader("token") String token) {
        User user = authService.loginCheck(token);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestHeader("name") String name,
                                      @RequestHeader("login") String login,
                                      @RequestHeader("pass") String password) {
        User user = authService.register(name,login,password);
        return ResponseEntity.ok(user);
    }

}
