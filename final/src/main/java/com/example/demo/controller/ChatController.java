package com.example.demo.controller;


import com.example.demo.model.Chat;
import com.example.demo.service.AuthService;
import com.example.demo.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/final")
public class ChatController {
    private final ChatService chatService;
    private final AuthService authService;
    @GetMapping
    public ResponseEntity<?> getAll(@RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(chatService.getAll());
        }
        return ResponseEntity.ok("Error:Sign in!");
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Chat chat,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.add(chat);
            return ResponseEntity.ok("Chat is added");
        }
        return ResponseEntity.ok("Error:Sign in!");

    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Chat chat,
                                  @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.update(chat);
            return ResponseEntity.ok(chat);
        }
        return ResponseEntity.ok("Error:Sign in!");

    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Chat chat,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.delete(chat);
            return ResponseEntity.ok("Chat is deleted");
        }
        return ResponseEntity.ok("Error:Sign in!");

    }
}
