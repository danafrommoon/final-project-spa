package com.example.demo.controller;


import com.example.demo.model.Message;
import com.example.demo.service.AuthService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final UserService userService;
    private final AuthService authService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Message message,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.add(message);
            return ResponseEntity.ok("Message is added");
        }
        return ResponseEntity.ok("Error:Sign in!");

    }


    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Message message,
                                  @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.update(message);
            return ResponseEntity.ok(message);
        }
        return ResponseEntity.ok("Error:Sign in!");

    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Message message,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.delete(message);
            return ResponseEntity.ok("Message is deleted");
        }
        return ResponseEntity.ok("Error:Sign in!");

    }


}

