package com.example.demo.controller;

import com.example.demo.model.Auth;
import com.example.demo.model.User;
import com.example.demo.service.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {

    private final AuthService authService;
    private final UserService userService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody User user) {
        userService.add(user);
        return ResponseEntity.ok("User is added");
    }
    @PostMapping
    public ResponseEntity<?> update(@RequestBody User user) {
        userService.update(user);
        return ResponseEntity.ok("User's info is updated'");
    }



    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> register(@RequestBody User user, @RequestBody Auth auth) {
        authService.login(auth);
        userService.add(user);
        return ResponseEntity.ok("User is registered");
    }

    @PutMapping
    public ResponseEntity<?> login(@RequestBody User user, @RequestBody Auth auth) {
        authService.update(auth);
        userService.update(user);
        return ResponseEntity.ok(user);
    }
    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody User user, @RequestBody Auth auth) {
        authService.delete(auth);
        userService.delete(user);
        return ResponseEntity.ok("User is deleted");
    }
}
