package com.example.demo.repository;


import com.example.demo.model.Auth;
import com.example.demo.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Chat, Long> {
    Auth getByToken(String token);
    Boolean existsByToken(String token);

    void save(Auth auth);

    void delete(Auth auth);

}
