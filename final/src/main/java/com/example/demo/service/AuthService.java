package com.example.demo.service;


import com.example.demo.model.Auth;
import com.example.demo.model.Chat;
import com.example.demo.model.User;
import com.example.demo.repository.AuthRepository;
import com.example.demo.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UserRepository userRepository;
    public void login(Auth auth) {
        UUID uuid = UUID.randomUUID();
        String uuidAsString = uuid.toString();
        Date date = new Date();
        auth.setToken(uuidAsString);
        authRepository.save(auth);
    }
    public Boolean isExistByToken(String token){
        return authRepository.existsByToken(token);
    }
    public User getUserByToken(String token){
        Auth auth = authRepository.getByToken(token);
        return userRepository.getUserById(auth.getUserId());
    }

    public List<Chat> getAll() {
        return authRepository.findAll();
    }

    public void update(Auth auth) {
        authRepository.save(auth);
    }

    public void delete(Auth auth) {
        authRepository.delete(auth);
    }

    public String login(String login, String password) {
        return login;
    }

    public void add(Auth auth) {
    }

    public User loginCheck(String token) {
        Auth auth = authRepository.getByToken(token);
        if (auth == null){
            return null;
        } else {
            return userRepository.getUserById(auth.getUserId());
        }
    }

    public User register(String name, String login, String password) {
       User user = userRepository.getUserByName(name);
        return user;
    }
}
