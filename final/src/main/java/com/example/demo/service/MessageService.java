package com.example.demo.service;
import com.example.demo.model.Message;
import com.example.demo.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public List<Message> getMessagesByChat(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    public void add(Message message) {
        Date date = new Date();
        messageRepository.save(message);
    }

    public void update(Message message) {
        Date date = new Date();
        messageRepository.save(message);
    }

    public void delete(Message message) {
        messageRepository.delete(message);
    }
}
